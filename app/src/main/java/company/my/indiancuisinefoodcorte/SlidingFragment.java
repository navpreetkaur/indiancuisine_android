package company.my.indiancuisinefoodcorte;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SlidingFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private int mDrawable;

    @BindView(R.id.imageView)
    ImageView imageView;

    public SlidingFragment() {
        // Required empty public constructor
    }

    public static SlidingFragment newInstance(int position) {
        SlidingFragment fragment = new SlidingFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDrawable = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sliding, container, false);

        ButterKnife.bind(this, view);

        if (getContext() != null){
            switch (mDrawable){
                case 0:
                    imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.d1));
                    break;
                case 1:
                    imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.d2));
                    break;
                case 2:
                    imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.d3));
                    break;
                case 3:
                    imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.d4));
                    break;
                case 4:
                    imageView.setImageDrawable(getContext().getResources().getDrawable(R.drawable.d5));
                    break;
            }
        }
        return view;
    }
}