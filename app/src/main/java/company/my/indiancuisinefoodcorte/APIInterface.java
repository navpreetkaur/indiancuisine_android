package company.my.indiancuisinefoodcorte;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by Navpreet Kaur on 2018-07-15.
 */
public interface APIInterface {

    String SNACK_DETAILS_KEY = "entry.1868567705";
    String CUSTOMER_NAME_KEY = "entry.2129011524";
    String CUSTOMER_EMAIL_KEY = "entry.1588044222";
    String CUSTOMER_PHONE_NUMBER = "entry.1249578496";
    String TOTAL_PRICE_KEY = "entry.1022597525";

    @POST
    @Multipart
    Call<ResponseBody> savePost(@Url String url,
                                @Header("Content-Type") String headerValue,
                                @Part(SNACK_DETAILS_KEY) RequestBody snackDetails,
                                @Part(CUSTOMER_NAME_KEY) RequestBody customerName,
                                @Part(CUSTOMER_EMAIL_KEY) RequestBody customerEmail,
                                @Part(CUSTOMER_PHONE_NUMBER) RequestBody customerPhoneNumber,
                                @Part(TOTAL_PRICE_KEY) RequestBody totalPrice);
}
