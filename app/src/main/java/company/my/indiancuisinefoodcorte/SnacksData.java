package company.my.indiancuisinefoodcorte;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Navpreet Kaur on 2018-07-10.
 */
public class SnacksData implements Parcelable {

    private int snackId;
    private String snackName;
    private double snackPrice;
    private int categoryId;

    private int quantity;

    public SnacksData(){}

    public SnacksData(int snackId, String snackName, double snackPrice, int categoryId) {
        this.snackId = snackId;
        this.snackName = snackName;
        this.snackPrice = snackPrice;
        this.categoryId = categoryId;
    }

    protected SnacksData(Parcel in) {
        snackId = in.readInt();
        snackName = in.readString();
        snackPrice = in.readDouble();
        categoryId = in.readInt();
        quantity = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(snackId);
        dest.writeString(snackName);
        dest.writeDouble(snackPrice);
        dest.writeInt(categoryId);
        dest.writeInt(quantity);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SnacksData> CREATOR = new Creator<SnacksData>() {
        @Override
        public SnacksData createFromParcel(Parcel in) {
            return new SnacksData(in);
        }

        @Override
        public SnacksData[] newArray(int size) {
            return new SnacksData[size];
        }
    };

    public int getSnackId() {
        return snackId;
    }

    public void setSnackId(int snackId) {
        this.snackId = snackId;
    }

    public String getSnackName() {
        return snackName;
    }

    public void setSnackName(String snackName) {
        this.snackName = snackName;
    }

    public double getSnackPrice() {
        return snackPrice;
    }

    public void setSnackPrice(double snackPrice) {
        this.snackPrice = snackPrice;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    ArrayList<SnacksData> getAllSnacks(){
        ArrayList<SnacksData> snacksDataArrayList = new ArrayList<>();

        SnacksData snacksData = new SnacksData(1, "Vegi Samosa", 2.99, 1);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(2, "Paneer Tikka", 9.99, 1);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(3, "Vegetable Pakora", 3.49, 1);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(4, "Chicken Samosa", 3.99, 1);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(5, "Bread/Pakora", 2.99, 1);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(6, "Riz cuit a la vapeur/Steam Rice", 1.99, 2);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(7, "Biryani de legumes/Vegetable Biryani", 6.99, 2);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(8, "Poulet Biryani/Chicken Briyani", 8.99, 2);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(9, "Beaf Briyani", 8.99, 2);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(10, "Cari d' Agneau/ Lamb Curry", 9.99, 3);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(11, "Agneau/Lamb Kadhai", 9.99, 3);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(12, "Agneau/Lamb Kabab", 7.99, 3);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(13, "Agneau/Lamb Tikka", 9.99, 3);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(14, "Saumon Masala/ Fish Masala", 12.99, 4);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(15, "Saumon Tikka/ Fish Tikka", 12.99, 4);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(16, "Curry De Poulet(sans os)/Chicken Curry(Boneless)", 9.99, 4);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(17, "Poulet Au Beurre(sans os)/Butter Chicken(Boneless)", 10.99, 5);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(18, "Poulet/Chicken Kadhai", 9.99, 5);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(19, "Poulet Masala avec Piment/ Chili Chicken", 10.99, 5);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(20, "Poulet Tikka Masala/Chicken Tikka Masala", 10.99, 5);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(21, "Poulet Palak/Palak Chicken", 9.99, 5);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(22, "Poulet/Chicken Kabab 2pcs.", 6.99, 5);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(23, "Poulet jambe 2pcs./Chicken Leg 2pcs.", 5.99, 5);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(24, "Curry Au Poulet avec du riz, yogourt, 2 chapaati ou naan et salade/Chicken Curry", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(25, "Kadhai poulet avec du riz, yogourt, 2 chapaati ou naan et salade/Kadhai Chicken", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksDataArrayList.add(snacksData);
        snacksData = new SnacksData(26, "Poulet Au beurre avec du riz, yogourt, 2 chapaati ou naan et salade/Butter Chicken", 15.99, 6);

        snacksData = new SnacksData(27, "Curry au porc avec du riz, yogourt, 2 chapaati ou naan et salade/pork Curry", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(28, "Kadhai porc avec du riz, yogourt, 2 chapaati ou naan et salade/pork Kadhai", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(29, "Curry d'agneau avec du riz, yogourt, 2 chapaati ou naan et salade/Lamb Curry", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(30, "Kadhai d'agneau avec du riz, yogourt, 2 chapaati ou naan et salade/Lamb Kadhai", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(31, "Curry du boeuf avec du riz, yogourt, 2 chapaati ou naan et salade/Beif Curry", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(32, "Kadhai boeuf avec du riz, yogourt, 2 chapaati ou naan et salade/Beif Kadhai", 15.99, 6);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(33, "Curry au poulet ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/ Chicken Curry", 8.99, 7);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(34, "poulet au beurre ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/Butter Chicken", 8.99, 7);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(35, "Curry au porc ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/ Pork Curry", 8.99, 7);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(36, "Curry d'agneau ou vegi 1 naan, riz, yogourt, salade et 1 boission gazeuse/Lamb Curry", 8.99, 7);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(37, "Plain Naan", 1.59, 8);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(38, "Ail Naan/Garlic Naan", 1.79, 8);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(39, "Chappati", 1.25, 8);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(40, "Rasgulla 1pc.", 1.49, 9);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(41, "Gulab Jamun 1pc.", 1.49, 9);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(42, "Mango Lassi", 2.99, 10);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(43, "Mango Secouer/Mango Shake", 2.99, 10);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(44, "Sweet & Salt Lassi", 1.99, 10);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(45, "Jus Mango/Mango Juice", 1.99, 10);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(46, "Jus Orange/Orange Juice", 1.99, 10);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(47, "Indian Tea", 1.79, 10);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(48, "Plat Mitonne Du Riz, Yogourt, 2 Chapaati Ou Naan Et Salade/mutter panner", 8.99, 11);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(49, "Epinards Paneer Du Riz, Yogourt, 2 Chapaati Ou Naan Et Salade/plak paneer", 8.99, 11);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(50, "Daal Makhni Du Riz, Yogourt, 2 Chapaati Ou Naan Et Salade/dal Makhni", 8.99, 11);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(51, "1 Pizza 12\" - \n +10 alies de poulet \n +ou 2 petites poutines \n +ou 2 petites salade cesar \n ou 1 moyenne lasagne", 20.99, 12);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(52, "1 Pizza 14\" - \n +15 alies de poulet \n +ou 4 petites poutines \n +ou 4 petites salade cesar", 25.99, 12);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(53, "1 Pizza 16\" - \n +10 alies de poulet \n +Frites \n +4 Pepsi 355 ml.", 29.99, 12);
        snacksDataArrayList.add(snacksData);

        snacksData = new SnacksData(54, "2 Pizza 12\" - \n +10 alies de poulet \n +Frites moyenne \n +4 Pepsi 355 ml.", 28.99, 12);
        snacksDataArrayList.add(snacksData);

        return snacksDataArrayList;
    }

    ArrayList<SnacksData> getSnacksWithCategory(int categoryId){
        ArrayList<SnacksData> snacksDataArrayList = getAllSnacks();
        ArrayList<SnacksData> filterSnacksList = new ArrayList<>();
        for (int i = 0; i < snacksDataArrayList.size(); i++) {
            if (snacksDataArrayList.get(i).getCategoryId() == categoryId){
                filterSnacksList.add(snacksDataArrayList.get(i));
            }
        }
        return filterSnacksList;
    }

}
