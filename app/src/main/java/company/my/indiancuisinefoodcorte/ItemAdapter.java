package company.my.indiancuisinefoodcorte;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.sectionedrecyclerview.ItemCoord;
import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Navpreet Kaur on 2018-07-11.
 */
public class ItemAdapter extends SectionedRecyclerViewAdapter<ItemAdapter.ViewHolder> {

    private ArrayList<SnacksData> snacksDataArrayListToOrder;
    private Context mContext;

    ItemAdapter(Context context){
        this.mContext = context;
        this.categoryDataArrayList = new CategoryData().getCategoryData();
        this.snacksDataArrayListToOrder = OrderDetailsArrayList.getSnacksDataArrayList();
    }

    private ArrayList<CategoryData> categoryDataArrayList;

    @Override
    public int getSectionCount() {
        if (categoryDataArrayList != null) {
            return categoryDataArrayList.size();
        }
        return 0;
    }

    @Override
    public int getItemCount(int sectionIndex) {
        int categoryId = categoryDataArrayList.get(sectionIndex).getCategoryId();
        ArrayList<SnacksData> snacksDataArrayList = new SnacksData().getSnacksWithCategory(categoryId);
        if (snacksDataArrayList != null) {
            return snacksDataArrayList.size();
        }
        return 0;
    }

    @Override
    public void onBindHeaderViewHolder(ItemAdapter.ViewHolder holder, int section, boolean expanded) {
        HeaderDashboardViewHolder headerDashboardViewHolder = (HeaderDashboardViewHolder) holder;
        headerDashboardViewHolder.headerText.setText(categoryDataArrayList.get(section).getCategoryName());
    }

    @Override
    public void onBindFooterViewHolder(ItemAdapter.ViewHolder holder, int section) {

    }

    @Override
    public void onBindViewHolder(ItemAdapter.ViewHolder holder, int section, int relativePosition, int absolutePosition) {
        RowDashboardViewHolder rowDashboardViewHolder = (RowDashboardViewHolder)holder;
        int categoryId = categoryDataArrayList.get(section).getCategoryId();
        ArrayList<SnacksData> snacksDataArrayList = new SnacksData().getSnacksWithCategory(categoryId);
        SnacksData snacksData = snacksDataArrayList.get(relativePosition);
        rowDashboardViewHolder.itemDescription.setText(snacksData.getSnackName());
        rowDashboardViewHolder.itemPrice.setText(String.valueOf("$"+snacksData.getSnackPrice()));
    }

    @NonNull
    @Override
    public ItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == VIEW_TYPE_HEADER) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_header_layout, parent, false);
            return new HeaderDashboardViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_layout, parent, false);
            return new RowDashboardViewHolder(v);
        }
    }

    class HeaderDashboardViewHolder extends ItemAdapter.ViewHolder {

        @BindView(R.id.headerText)
        TextView headerText;

        HeaderDashboardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class RowDashboardViewHolder extends ItemAdapter.ViewHolder {

        @BindView(R.id.itemDescription)
        TextView itemDescription;

        @BindView(R.id.itemPrice)
        TextView itemPrice;

        @BindView(R.id.addButton)
        TextView addButton;

        @OnClick(R.id.addButton)
        void addButtonPressed() {
            ItemCoord position = getRelativePosition();
            int section = position.section();
            int relativePosition = position.relativePos();

            int categoryId = categoryDataArrayList.get(section).getCategoryId();
            ArrayList<SnacksData> snacksDataArrayList = new SnacksData().getSnacksWithCategory(categoryId);
            SnacksData snacksData = snacksDataArrayList.get(relativePosition);

            if (snacksDataArrayListToOrder == null){
                snacksDataArrayListToOrder = new ArrayList<>();
            }

            if (!snacksDataArrayListToOrder.contains(snacksData)){
                snacksDataArrayListToOrder.add(snacksData);
                OrderDetailsArrayList.setSnacksDataArrayList(snacksDataArrayListToOrder);
            } else {
                Toast.makeText(mContext, "This item has already been added to the cart. You can increase the quantity from the cart.", Toast.LENGTH_SHORT).show();
            }
        }

        RowDashboardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ViewHolder extends SectionedViewHolder{

        ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}