package company.my.indiancuisinefoodcorte;

/**
 * Created by Navpreet Kaur on 2018-07-15.
 */
public class APIUtils {
    private APIUtils() {}

    private static final String BASE_URL = "https://docs.google.com/";

    public static APIInterface getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIInterface.class);
    }
}
