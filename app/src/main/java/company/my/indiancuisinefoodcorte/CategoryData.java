package company.my.indiancuisinefoodcorte;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Navpreet Kaur on 2018-07-10.
 */
public class CategoryData implements Parcelable{

    private int categoryId;
    private String categoryName;

    public CategoryData(){}

    public CategoryData(int categoryId, String categoryName){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    protected CategoryData(Parcel in) {
        categoryId = in.readInt();
        categoryName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(categoryId);
        dest.writeString(categoryName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryData> CREATOR = new Creator<CategoryData>() {
        @Override
        public CategoryData createFromParcel(Parcel in) {
            return new CategoryData(in);
        }

        @Override
        public CategoryData[] newArray(int size) {
            return new CategoryData[size];
        }
    };

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    public ArrayList<CategoryData> getCategoryData(){
        ArrayList<CategoryData> categoryDataArrayList = new ArrayList<>();

        CategoryData categoryData = new CategoryData(1, "Snacks/Appetizers");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(2, "RIZ/RICE");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(3, "AGNEAU/LAMB");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(4, "FRUITS DE MER/SEAFOOD");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(5, "POULET/CHICKEN");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(6, "POUR DEUX PERSONNES/COMBO SPECIAL");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(7, "POUR I PERSONNE/SPECIAL THALI");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(8, "PAINS INDIENS/INDIAN BREADS");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(9, "DESSERTS");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(10, "BOISSOINS/DRINKS");
        categoryDataArrayList.add(categoryData);

        categoryData = new CategoryData(11, "POUR I PERSONNE/VEGETARIAN SPECIAL THALI");
        categoryDataArrayList.add(categoryData);

        return categoryDataArrayList;

    }
}
