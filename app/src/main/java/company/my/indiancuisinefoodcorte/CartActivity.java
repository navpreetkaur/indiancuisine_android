package company.my.indiancuisinefoodcorte;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CartActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.totalPrice)
    TextView totalPriceTextView;

    @OnClick(R.id.orderDetailsButton)
    void orderDetailButtonPressed() {
        if (snacksDataArrayList != null && snacksDataArrayList.size() > 0) {
            OrderDetailsArrayList.setSnacksDataArrayList(snacksDataArrayList);
            Intent intent = new Intent(this, OrderDetailsActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please add an item first.", Toast.LENGTH_SHORT).show();
        }

    }

    private ArrayList<SnacksData> snacksDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar;

        if (getSupportActionBar() != null) {
            actionBar = getSupportActionBar();
            actionBar.setTitle("Cart");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        ButterKnife.bind(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        snacksDataArrayList = OrderDetailsArrayList.getSnacksDataArrayList();
        if (snacksDataArrayList != null) {
            calculatePrice();
            ItemOrderedAdapter itemOrderedAdapter = new ItemOrderedAdapter(snacksDataArrayList);
            recyclerView.setAdapter(itemOrderedAdapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class ItemOrderedAdapter extends RecyclerView.Adapter<ItemOrderedAdapter.OrderedItemViewHolder> {

        private ArrayList<SnacksData> snacksDataArrayList;

        ItemOrderedAdapter(ArrayList<SnacksData> snacksDataArrayList) {
            this.snacksDataArrayList = snacksDataArrayList;
        }

        @NonNull
        @Override
        public OrderedItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cart_layout, parent, false);
            return new OrderedItemViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull OrderedItemViewHolder holder, int position) {
            SnacksData snacksData = snacksDataArrayList.get(position);
            holder.itemDescription.setText(snacksData.getSnackName());
            holder.itemPrice.setText(String.valueOf("$" + snacksData.getSnackPrice()));
            if (snacksData.getQuantity() <= 0) {
                holder.quantity.setText("1");
            } else {
                holder.quantity.setText(String.valueOf(snacksData.getQuantity()));
            }
        }

        @Override
        public int getItemCount() {
            if (snacksDataArrayList != null) {
                return snacksDataArrayList.size();
            }
            return 0;
        }

        class OrderedItemViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.itemPrice)
            TextView itemPrice;

            @BindView(R.id.itemDescription)
            TextView itemDescription;

            @BindView(R.id.quantity)
            TextView quantity;

            @BindView(R.id.deleteButton)
            TextView deleteButton;

            @OnClick(R.id.increaseCount)
            void increaseCounterPressed() {
                if (snacksDataArrayList != null) {
                    int quantity = snacksDataArrayList.get(getAdapterPosition()).getQuantity();
                    quantity++;
                    snacksDataArrayList.get(getAdapterPosition()).setQuantity(quantity);
                    calculatePrice();
                    notifyDataSetChanged();
                }
            }

            @OnClick(R.id.decreaseCount)
            void decreaseCounterPressed() {
                if (snacksDataArrayList != null) {
                    int quantity = snacksDataArrayList.get(getAdapterPosition()).getQuantity();
                    if (quantity > 1) {
                        quantity--;
                        snacksDataArrayList.get(getAdapterPosition()).setQuantity(quantity);
                        calculatePrice();
                        notifyDataSetChanged();
                    }
                }
            }

            @OnClick(R.id.deleteButton)
            void deleteButtonPressed() {
                if (snacksDataArrayList != null && snacksDataArrayList.size() > getAdapterPosition()) {
                    snacksDataArrayList.remove(getAdapterPosition());
                    calculatePrice();
                    notifyDataSetChanged();
                }
            }

            OrderedItemViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    private void calculatePrice() {
        double totalPrice = 0;
        int quantity = 0;
        double price = 0;
        for (int i = 0; i < snacksDataArrayList.size(); i++) {
            quantity = snacksDataArrayList.get(i).getQuantity();
            price = snacksDataArrayList.get(i).getSnackPrice();
            if (quantity <= 0) {
                quantity = 1;
            }
            totalPrice += (price * quantity);
        }

        totalPriceTextView.setText(String.format(Locale.CANADA, "$%.2f", totalPrice));
    }
}