package company.my.indiancuisinefoodcorte;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;

public class OrderDetailsActivity extends AppCompatActivity {

    String urlString = "https://docs.google.com/forms/d/e/1FAIpQLSdcfOLbjr3pgg24bxS4lDYGlVWpVGk-9gSoaISKsQ_DXk72jw/viewform";
    String SNACK_DETAILS_KEY = "entry.1868567705";
    String CUSTOMER_NAME_KEY = "entry.2129011524";
    String CUSTOMER_EMAIL_KEY = "entry.1588044222";
    String CUSTOMER_PHONE_NUMBER = "entry.1249578496";
    String TOTAL_PRICE_KEY = "entry.1022597525";
    public static final MediaType FORM_DATA_TYPE
            = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

    @BindView(R.id.firstName)
    EditText firstName;

    @BindView(R.id.lastName)
    EditText lastName;

    @BindView(R.id.emailAddress)
    EditText emailAddress;

    @BindView(R.id.phoneNumber)
    EditText phoneNumber;

    @OnClick(R.id.orderButton)
    void orderButtonPressed(){
        if (isInvalidString(firstName.getText().toString().trim())){
            Toast.makeText(this, "Please enter first name", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isInvalidString(lastName.getText().toString().trim())){
            Toast.makeText(this, "Please enter last name", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isInvalidString(emailAddress.getText().toString().trim())){
            Toast.makeText(this, "Please enter email Address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isInvalidString(phoneNumber.getText().toString().trim())){
            Toast.makeText(this, "Please enter phone Number", Toast.LENGTH_SHORT).show();
            return;
        }

        orderNow();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);
    }

    private boolean isInvalidString(String text){
        return text == null || (text.equalsIgnoreCase(""));
    }

    private void orderNow(){
        String snackDetails = "";

        ArrayList<SnacksData> snacksDataArrayList = OrderDetailsArrayList.getSnacksDataArrayList();
        int size = snacksDataArrayList.size();
        double totalPrice = 0.0;
        SnacksData snacksData;
        for (int i = 0; i < size; i++) {
            snacksData = snacksDataArrayList.get(i);
            snackDetails = snackDetails.concat("\n\n\n============"+(i + 1)+"=================");
            snackDetails = snackDetails + "ID-"+snacksData.getSnackId() +"\nName-"+snacksData.getSnackName()+"\nQuantity"+snacksData.getQuantity()+"\nPrice/Pc"+snacksData.getSnackPrice();
            int q = snacksData.getQuantity();
            if (q <= 0){
                q = 1;
            }
            totalPrice += (q * snacksData.getSnackPrice());
        }

        sendPost(snackDetails, "$"+totalPrice);
    }

    private void sendPost(String snackDetails, String totalPrice) {
        postData(snackDetails, totalPrice);
//        String customerName = firstName.getText().toString().trim() +" "+lastName.getText().toString().trim();
//        String emailAddressString = emailAddress.getText().toString().trim();
//        String phoneNumberString = phoneNumber.getText().toString().trim();

//        try
//        {
//            URL url = new URL(urlString);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setReadTimeout(60000);
//            conn.setConnectTimeout(60000);
//            conn.setRequestMethod("POST");
//            conn.setDoInput(true);
//            conn.setRequestProperty("Cache-Control", "no-cache");
//
//            String data = CUSTOMER_NAME_KEY+"=" + URLEncoder.encode(customerName) + "&" +
//                    CUSTOMER_PHONE_NUMBER+"=" + URLEncoder.encode(phoneNumberString) + "&" +
//                    SNACK_DETAILS_KEY+"=" + URLEncoder.encode(snackDetails) + "&" +
//                    TOTAL_PRICE_KEY+"=" + URLEncoder.encode(totalPrice) + "&" +
//                    CUSTOMER_EMAIL_KEY+"=" + URLEncoder.encode(emailAddressString);
//
//
//
//        } catch (Exception e){
//            e.printStackTrace();
//        }


//        RequestBody customerNamebody = RequestBody.create(MediaType.parse("text/plain"), customerName);
//        RequestBody emailAddressStringbody = RequestBody.create(MediaType.parse("text/plain"), emailAddressString);
//        RequestBody phoneNumberStringbody = RequestBody.create(MediaType.parse("text/plain"), phoneNumberString);
//        RequestBody snackDetailsbody = RequestBody.create(MediaType.parse("text/plain"), snackDetails);
//        RequestBody totalPricebody = RequestBody.create(MediaType.parse("text/plain"), totalPrice);
//        APIInterface mAPIService = APIUtils.getAPIService();
//        mAPIService.savePost("https://docs.google.com/forms/d/e/1FAIpQLSdcfOLbjr3pgg24bxS4lDYGlVWpVGk-9gSoaISKsQ_DXk72jw/viewform","text/html",snackDetailsbody, customerNamebody, emailAddressStringbody,phoneNumberStringbody, totalPricebody).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
//
//                if(response.isSuccessful()) {
//                    if (response.body() != null){
//                        OrderDetailsArrayList.getSnacksDataArrayList().clear();
//                        Intent intent = new Intent(OrderDetailsActivity.this, PickUpActivity.class);
//                        startActivity(intent);
//                    } else {
//                        Toast.makeText(OrderDetailsActivity.this, "Error occurred while placing order. Please call and place your order.", Toast.LENGTH_SHORT).show();
//                    }
//                } else {
//                    Toast.makeText(OrderDetailsActivity.this, "Error occurred while placing order. Please call and place your order.", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
//                Toast.makeText(OrderDetailsActivity.this, "Error occurred while placing order. Please call and place your order.", Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    public void postData(final String snackDetails, final String totalPrice) {

        final String customerName = firstName.getText().toString().trim() +" "+lastName.getText().toString().trim();
        final String emailAddressString = emailAddress.getText().toString().trim();
        final String phoneNumberString = phoneNumber.getText().toString().trim();

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                urlString,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("TAG", "Response: " + response);
                        if (response.length() > 0) {

                            Toast.makeText(OrderDetailsActivity.this, "Successfully Posted", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailsActivity.this, "Error occurred while placing order. Please call and place your order.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrderDetailsActivity.this, "Error occurred while placing order. Please call and place your order.", Toast.LENGTH_SHORT).show();
            }
        }
        ){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(CUSTOMER_EMAIL_KEY, emailAddressString);
                params.put(CUSTOMER_NAME_KEY, customerName);
                params.put(CUSTOMER_PHONE_NUMBER, phoneNumberString);
                params.put(TOTAL_PRICE_KEY, totalPrice);
                params.put(SNACK_DETAILS_KEY, snackDetails);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }
}
