package company.my.indiancuisinefoodcorte;

import android.app.Application;
import android.content.Context;

/**
 * Created by Navpreet Kaur on 2018-06-25.
 */
public class MyApplication extends Application {

    private static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
    }

    public static MyApplication getInstance() {
        return instance;
    }

    public static Context getAppContext() {
        return instance.getApplicationContext();
    }
}