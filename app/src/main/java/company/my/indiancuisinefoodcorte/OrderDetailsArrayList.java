package company.my.indiancuisinefoodcorte;

import java.util.ArrayList;

/**
 * Created by Navpreet Kaur on 2018-07-15.
 */
public class OrderDetailsArrayList {

    private static ArrayList<SnacksData> snacksDataArrayList;

    public static ArrayList<SnacksData> getSnacksDataArrayList() {
        return snacksDataArrayList;
    }

    public static void setSnacksDataArrayList(ArrayList<SnacksData> snacksDataArrayList) {
        OrderDetailsArrayList.snacksDataArrayList = snacksDataArrayList;
    }
}
